<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              liconoclasta.it
 * @since             1.0.2
 * @package           Telebot
 *
 * @wordpress-plugin
 * Plugin Name:       Telebot
 * Plugin URI:        liconoclasta.it
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.2
 * Author:            Max
 * Author URI:        liconoclasta.it
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       telebot
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'TELEBOT_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-telebot-activator.php
 */
function activate_telebot() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-telebot-activator.php';
	Telebot_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-telebot-deactivator.php
 */
function deactivate_telebot() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-telebot-deactivator.php';
	Telebot_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_telebot' );
register_deactivation_hook( __FILE__, 'deactivate_telebot' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-telebot.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_telebot() {

	$plugin = new Telebot();
	$plugin->run();

}
run_telebot();


add_action( 'rest_api_init', function () {
  register_rest_route( 'api/v1', '/tg/', array(
    'methods'  => 'POST',
    'callback' => 'bot_manager',
  ) );
} );

function bot_manager( $request ) {

	$json     = $request->get_json_params();
	$msg      = $json['message'];
	$group_id = 0;

	//if( $msg['chat']['id'] == $group_id && class_exists( 'WPTelegram\BotAPI\API' ) ) {

		file_put_contents( $_SERVER['DOCUMENT_ROOT'] . "/test", print_r($request,true) );

		switch( $msg['text'] ) {
			case '/test':

				break;
		}


	//}

}
