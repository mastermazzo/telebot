<?php

/**
 * Fired during plugin deactivation
 *
 * @link       liconoclasta.it
 * @since      1.0.0
 *
 * @package    Telebot
 * @subpackage Telebot/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Telebot
 * @subpackage Telebot/includes
 * @author     Max <mastermazzo@tuta.io>
 */
class Telebot_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
