<?php

/**
 * Fired during plugin activation
 *
 * @link       liconoclasta.it
 * @since      1.0.0
 *
 * @package    Telebot
 * @subpackage Telebot/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Telebot
 * @subpackage Telebot/includes
 * @author     Max <mastermazzo@tuta.io>
 */
class Telebot_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
